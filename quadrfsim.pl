#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig ':radial';
use Data::Dumper;


my $f = 5e10;
my $rf = 1e-11/$f; # strength of radio frequency charge
my $cv = $rf/10; # strength of constant charge
my $t = 0;
my $dt = 0.1;
my $d = 0.02;
my $r = 0.01;
my $l = $d-$r;
my @rods = standard_rods($d, $r);
my $ion ;

foreach my $e(6..18){
	my $m = int(1.5**$e);
	$ion = ion($m,1);
	my $thru = 1;
	open(my $fh, '>', "$m.tsv") or die $!;
	foreach(1..1e4){
		@rods = setRods($rf, $cv, $t, @rods);
		$ion = doIonInField($ion, $f, @rods);
		if($ion->[2] < -$l || $ion->[2] > $l || $ion->[3] < -$l || $ion->[3] > $l){
			$thru = 0;
			last;
		}
		print $fh join("\t",$t,$ion->[2]/$l,$ion->[3]/$l,$rods[0]->[2],$rods[0]->[1],$rods[2]->[2],$rods[3]->[2]);
		print $fh "\n";
		$t += $dt;
	}
	close($fh);
	printf "%d\t%d\n", $m, $thru;
}

sub K {
	return 9.0e9;
}

sub ion {
	my ($m, $z) = @_;
	return [$m, $z, 0.00001, 0.00001, 0, 0]; # molecular mass, charge, x and y posn, x and y velocity
}

sub standard_rods {
	my ($d, $r) = @_;
	return (
		[ -$d, 0, 1, $r, 'p' ],
		[ $d, 0, 1, $r, 'p' ],
		[ 0, -$d, -1, $r, 'n' ],
		[ 0, $d, -1, $r, 'n' ],
	);
}

sub setRods {
	my ($rf, $cv, $t, @rods) = @_;
	my $q = $cv + $rf * cos($t);
	for(my $i=0; $i<@rods; $i++){
		if($rods[$i]->[4] eq 'p'){
			$rods[$i]->[2] = $q;
		}
		else {
			$rods[$i]->[2] = -$q;
		}
	}
	return @rods;
}

sub doIonInField {
	my ($ion, $f, @rods) = @_;
	my $field = fieldAtPoint($ion->[2],$ion->[3], @rods);
	return accelIon($ion, $field, $f);
}

sub fieldAtPoint {
	my ($x,$y,@Rods) = @_;
	my @cart = (0,0);
	foreach my $Rod(@Rods){
		my ($X,$Y,$Q,$R) = @$Rod; # careful now, Q is charge! R is radius of rod
		my ($rho, $theta, $z)     = cartesian_to_cylindrical($X-$x, $Y-$y, 0);
		my $rfs = K()*$Q/(($rho-$R)**2);
		my ($xcomp,$ycomp,$zcomp) = cylindrical_to_cartesian($rfs, $theta);
		$cart[0] += $xcomp * 9e9;
		$cart[1] += $ycomp * 9e9;
	} 
	return [@cart];
}

sub accelIon {
	my ($ion, $field) = @_;
	my @ion = @$ion; # a copy
	my $q = $ion[1] / 6.24150934e18;
	my $m = $ion[0] / 6.022e23;
	my $Fx = $q * $field->[0];
	my $Fy = $q * $field->[1];
	my $ax = $Fx / $m;
	my $ay = $Fy / $m;
	#print "@$field $Fx $Fy $ax $ay\n";
	$ion[4] += $ax;
	$ion[5] += $ay;
	$ion[2] += $ion[4]/$f;
	$ion[3] += $ion[5]/$f;
	return \@ion;
}

